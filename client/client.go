package client


import (
    "fmt"
    "strings"
    "io/ioutil"
    "errors"
    "net/http"
    "github.com/golang/glog"
)

// Client transactions, as they are logged in-memory on every node
type TX struct {
    Body []byte
    Id   string
}

// Client requests propagate from node to node along the chain
type TXRequest struct {
    Body   []byte
    Id     string
    Client string
}

// GetClientNumericHash returns a uint number identifying in a non-unique way
// the Client (`cr.Client` field) who issued this transaction request.
func (cr *TXRequest) GetClientNumericHash() uint {
    if cr.Client == "" {
        return 0
    }

    // cr.Client is an IP represented in d.d.d.d form as a string
    var tk [4]uint
    // Extract the four numbers of the IP
    if _, err := fmt.Sscanf(cr.Client, "%d.%d.%d.%d", &tk[0], &tk[1], &tk[2], &tk[3]); err == nil {
        var s uint = 0
        for _, token := range tk {
            s += token
        }
        glog.Infof("client.TXRequest from %s has numeric hash %d", cr.Client, s)
        return s
    }
    return 0
}


// Parses a client's HTTP request and returns a corresponding client.TXRequest
func ParseTXRequest(r *http.Request) (cr TXRequest, err error) {
    // Get the ID from the request URL
    cr.Id = strings.Split(r.URL.Path, "/")[2]
    if len(cr.Id) <= 0 {
        return cr, errors.New("TX id missing")
    }

    // The transaction body is formed from the request body
    defer r.Body.Close()
    cr.Body, err = ioutil.ReadAll(r.Body)
    if err != nil {
        glog.Warningf("Error reading the TX body!(%s)", err)
        return
    }

    // Remember the client's addr to reply after the TX is persisted
    // We can ignore the port, only need the IP
    cr.Client = strings.Split(r.RemoteAddr, ":")[0]
    return cr, nil
}