# chainr

A basic chain replication service.

## Properties & limitations

* Ephemeral (in-memory) storage
* Static replicas configuration: no failures nor reconfiguration handled at the moment
* Stateless HTTP is the only form of communication between (and among) nodes and clients
* Closed-loop: the client gets a response to a call only after all nodes in the chain -- except for the tail -- finished propagating the associated TX.
* Assumes that transactions have a textual representation and do not contain commas (',') or newlines ('\n'). The problem is with the request serialization, which has a naive implementation relying on these special characters.

## Configuration file

Upon starting a node, we need to pass the configuration through a JSON file, which we specify using the `-cfg FILE` argument.

All the parameters are optional:

    NextReplicaAddr string
    NumCarriers     int
    RenewalPeriod   int
    BodyMaxSizeKB   int
    MaxOutstanding  int

Most of these parameters have defaults -- except for NextReplicaAddr:

    defaultNumCarriers     = 10     // How many parallel carriers should we use
    defaultRenewalPeriod   = 5      // How many seconds to wait between
                                    // subsequent HTTP request renewal
    defaultBodyMaxSizeKB   = 1024   // The upper limit, in KB of the overall
                                    // size of chunked HTTP requests
    defaultMaxOutstanding  = 1024   // Maximum # of outstanding TXs a carrier
                                    // may have; outstanding = enqueued but
                                    // not yet propagated to the next node

In its most basic form, the configuration should look like:

    {
        "NextReplicaAddr": "192.168.0.1:8080"
    }

In case NextReplicaAddr is missing, then the node is considered to be the tail of the chain.

## RESTful API

##### Writing a new transaction with id 'ff0044aa'

    PUT /tx/ff0044aa
    Host: 192.168.99.100:8080
    Content-Type: application/json

    { "body": "transactionxx" }

Expected outcome:

    HTTP/1.1 202 Accepted
    Content-Type: text/plain
    Date: Tue, 23 Aug 2016 09:08:26 GMT
    Content-Length: 7

    queued

We can easily simulate a client with the following:

    wget -t1 -S --method=PUT -q -O - \
        --header='Content-Type: application/octet-stream' \
        --body-data='xxx' "http://192.168.99.100:8080/tx/200"

This creates a new transaction with ID *200*.

##### Internal API that is used to propagate data among nodes in the chain

Assume a client does a transaction with id 'ff0044aa' and body 'transactionxx', the following HTTP request propagates the transaction

    POST /txs
    Host: 192.168.99.100:8080
    TransferEncoding: chunked

    ff0044aa,transactionxx


## Compiling and running with Docker

Build the image; note that this also compiles the code:

    cd bitbucket.org/adizere/chainr
    docker build -t chainr .

Compile the code -- this is only needed if the code was changed

    docker run -P --rm  \
        -v "$PWD":/usr/src/chainr \
        -w /usr/src/chainr --name chainr \
        chainr go build -v

Now run three nodes from three separate consoles. We'll be using the configuration files from `config/`. Note that these should be modified (at least the first two), to account for node IP addresses.

##### First node.

This is the only node whose port we need to expose (also note the `EXPOSE` directive in the Dockerfile):

    docker run -p 8080:8080 --rm \
        -v "$PWD":/usr/src/chainr \
        -w /usr/src/chainr \
        --name chainr-node-1 chainr \
        ./chainr -logtostderr=true -cfg config/chain-node-1.json

Note that option `-p 8080:8080` is especially relevant here; it binds the container's port *8080* to be accessible from within the host on the same port. See more details [here](https://docs.docker.com/engine/reference/run/#expose-incoming-ports). We need this to be able to invoke client requests on the service.

To check that everything went fine with the exposed port, you can use `docker ps | grep chainr`. This should include the following
> 0.0.0.0:8080->8080/tcp

So the associated port on the host would be the same -- `8080`.

**Important**:
To get the host's ip, we'll need `docker-machine ls`, which will tell us something along the lines of:

    URL                         SWARM   DOCKER    ERRORS
    default   *        virtualbox   Running   tcp://192.168.99.100:2376

So the IP we're looking for -- which we'll be using to invoke the service -- is `192.168.99.100`. We'll need this after all three nodes are up and running.

##### Second node.

    docker run --rm \
        -v "$PWD":/usr/src/chainr -w /usr/src/chainr \
        --name chainr-node-2 chainr \
        ./chainr -logtostderr=true -cfg=config/chain-node-2.json

##### Third node.

Similarly:

    docker run --rm \
        -v "$PWD":/usr/src/chainr -w /usr/src/chainr \
        --name chainr-node-3 chainr \
        ./chainr -logtostderr=true -cfg=config/chain-node-3.json

The final test to check that everything runs properly is to invoke an operation in the API. This is where the IP we were searching for before, `192.168.99.100`, comes into play.

    wget -t1 -S --method=PUT -q -O - \
        --header='Content-Type: application/octet-stream' \
        --body-data='xxx' \
        "http://192.168.99.100:8080/tx/200"

Which should result in:

      HTTP/1.1 202 Accepted
      Content-Type: text/plain
      Date: Wed, 24 Aug 2016 14:11:50 GMT
      Content-Length: 7
    queued