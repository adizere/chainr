/*
A basic chain replication service.

Sample usage:

    ./chainr -logtostderr=true -cfg config/chain-node-1.json


Notes:
-- glog, a logging facility
    - provides funcs Info, Warning, Error, Fatal,
    - plus formatting variants such as Infof
    - example: glog.Fatalf("Initialization failed: %s", err)
*/

package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"flag"
	"github.com/golang/glog"
	"gopkg.in/tylerb/graceful.v1"
	"io"
	"io/ioutil"
	"math/rand"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"
	"bitbucket.org/adizere/chainr/client"
)

const (
	logInitialSize = 1024 // Initial capacity of the in-memory log store
	tearDownSlack  = 2    // Second to wait for the HTTP layer to close

	// Some connection constants
	defaultListenAddr = ":8080"
	successStatusCode = 202 // HTTP status code for an accepted TX
	errorStatusCode   = 400 // HTTP status code for an erroneous TX

	// Some constants for the inter-node carrier process
	// These are defaults & prone to be overwritten by the config file
	defaultNumCarriers   = 10 // How many parallel carriers should we use
	defaultRenewalPeriod = 1  // How many seconds to wait between
	// subsequent HTTP request renewal
	defaultBodyMaxSizeKB = 1024 // The upper limit, in KB of the overall
	// size of chunked HTTP requests
	defaultMaxOutstanding = 1024 // Maximum # of outstanding TXs a carrier
	// may have; outstanding = enqueued but
	// not yet propagated to the next node

	// Constants for the swinger routines; these routines "close the chain",
	// by informing clients about the termination of their requests
	defaultNumSwingers  = 10
	defaultAckTimeoutMs = 100
)

type chainrConf struct {
	listenAddr      string
	cfgPath         string
	NumCarriers     int
	RenewalPeriod   int
	NextReplicaAddr string
	BodyMaxSizeKB   int
	MaxOutstanding  int
}


// The carrier handles propagation of TX between the local node and the next
// one in the chain
type carrier struct {
	nr          int                   // An identification #, for easier debugging
	outstanding chan *client.TXRequest // Channel holds incoming client TXs
	// before the carrier propagates them
	target string // The target of the carrier is the next
	// replica in the chain
	httpClient *http.Client // The node's http client; propagates TXs
}

type swinger struct {
	nr          int                   // An identification #, for easier debugging
	outstanding chan *client.TXRequest // Channel holds finished client TXs
	// before the swinger acknowledges
	// them to clients
	httpClient *http.Client // The node's http client; for sending
	// acknowledgements to clients
}

type chainrNode struct {
	conf     *chainrConf // The configuration for the local node
	carriers []carrier   // Handle TX propagation among replicas
	swingers []swinger   // Handle TX acknowledgements to clients
	log      []client.TX  // For the log itself, go-devs seems to praise
	// slices, so let's use them
}

// the web server handler of the local chainrNode
func (node *chainrNode) ServeHTTP(w http.ResponseWriter, r *http.Request) {

	switch r.Method {
	// This is a propagated request comming from the previous node in the chain
	case "POST":
		defer r.Body.Close()
		s := bufio.NewScanner(r.Body)
		err := node.logPropagatedTXs(s)
		if err != nil {
			glog.Fatalf("Error logging propagated TXs: %s", err)
			node.respondHTTPError(w)
		} else {
			node.respondHTTPSuccess(w)
		}

	// This is a client's request
	case "PUT":
		// Parse the transaction
		cr, err := client.ParseTXRequest(r)
		if err != nil {
			glog.Errorf("Error parsing client's request: %s;", err)
			node.respondHTTPError(w)
		} else {
			// Log the transaction to the local in-memory store & save it for
			// propagation down the chain
			node.logClientTXRequest(cr)
			node.respondHTTPSuccess(w)
		}
	}
}

func (node *chainrNode) respondHTTPSuccess(w http.ResponseWriter) {
	w.Header().Set("Content-Type", "text/plain")
	w.WriteHeader(successStatusCode)
	io.WriteString(w, "queued\n")
}

func (node *chainrNode) respondHTTPError(w http.ResponseWriter) {
	w.WriteHeader(errorStatusCode)
	io.WriteString(w, "malformed request\n")
}

// This method logs a client TX request to the local node's log and then
// forwards the TX request to the carrier which will handle propagation
func (node *chainrNode) logClientTXRequest(cr client.TXRequest) {
	node.logTXRequestLocally(cr)
	node.registerOutstandingTX(&cr)
}

// This method logs propagated TXs to the local node's log, and then hands them
// over to the carrier, which will continue propagation to next replicas in th
// chain
func (node *chainrNode) logPropagatedTXs(s *bufio.Scanner) error {
	glog.Infof("Scanning a new batch..")

	var tot int = 0
	for s.Scan() {
		tot++
		tx := deserializeClientTXRequest(s.Bytes())
		node.logClientTXRequest(*tx)
	}
	if err := s.Err(); err != nil {
		glog.Warningf("Error scanning a batch:", err)
		return err
	}
	glog.Infof("+ %d transactions", tot)
	return nil
}

// Extracts the transaction and logs it in the in-memory log
func (node *chainrNode) logTXRequestLocally(cr client.TXRequest) {
	tx := &client.TX{
		Body: cr.Body,
		Id:   cr.Id,
	}
	// Append to the in-memory log
	node.log = append(node.log, *tx)

	// Report on the log size every once in a while
	if len(node.log)%100 == 0 {
		glog.Infof("Log size=%d", len(node.log))
	}
}

func (node *chainrNode) registerOutstandingTX(cr *client.TXRequest) {
	// Put the transaction in the carrier's incoming TX channel
	if node.carriers != nil {
		// This is a node inside the chain (i.e., not the tail)
		// Get a random carrier and assign the tx request to it
		node.carriers[rand.Intn(node.conf.NumCarriers)].outstanding <- cr
	} else {
		// This is the tail, we must notify the client that we finished
		// persisting the TX; this is the role of swingers.
		// We'll select the swinger based on the client info
		sid := cr.GetClientNumericHash() % defaultNumSwingers
		node.swingers[sid].outstanding <- cr
	}
}

// The carrier's main functionality -- consume incoming TX and propagate them.
// All the heavy-weight lifting around HTTP chunked transfer instrumentation is
// around here: this method tries hard to reduce the number of outgoing HTTP
// requests.
func (carrier *carrier) run(renewalPeriod time.Duration, bodyMaxSizeKB int) {
	glog.Infof("[%d] carrier started", carrier.nr)

	if carrier.target == "" {
		// This should happen
		glog.Fatalf("We're the tail. No need to propagate anything.")
	}

	writer := carrier.renewChunkedHTTPRequest()

	var lastBodyTotal int = 0

	// Use a ticker to periodically renew HTTP requests, such that we avoid
	// getting into any issues, e.g., upper bounds on HTTP request sizes,
	// exceeding buffer sizes, timeouts, hanging requests, etc.
	tickerChannel := time.Tick(renewalPeriod * time.Second)

	for {
		select {
		case newClientTX := <-carrier.outstanding:
			n, err := writer.Write(serializeClientTXRequest(newClientTX))
			if err != nil {
				// If we cannot write, then renew the HTTP request.
				// This happened b/c chunked request raised an error, see
				// outgoingChunkedRequest().
				lastBodyTotal = 0
				writer = carrier.renewChunkedHTTPRequest()

				// Enqueue the transaction, we'll retry it later
				carrier.outstanding <- newClientTX
				// Start anew.
				continue
			}
			lastBodyTotal += n
			// Renew the HTTP request
			if lastBodyTotal >= 1024*bodyMaxSizeKB {
				glog.Infof("[%d] Max body reached; ending request at %.2f KB",
					carrier.nr, float64(lastBodyTotal)/1024.0)
				lastBodyTotal = 0
				writer.Close()
				writer = carrier.renewChunkedHTTPRequest()
			}
		case <-tickerChannel:
			// With every tick, we renew this carrier's HTTP channel:
			// finish-off the last chunked request and start a new one
			// Renew the HTTP request
			if lastBodyTotal > 0 {
				glog.Infof("[%d] Ticked; ending request at %.2f KB",
					carrier.nr, float64(lastBodyTotal)/1024.0)
				lastBodyTotal = 0
				writer.Close()
				writer = carrier.renewChunkedHTTPRequest()
			}
		}
	}
}

// Mostly based on https://play.golang.org/p/CZaWdr4oft
// See also comments from https://github.com/golang/go/issues/6574
func (carrier *carrier) renewChunkedHTTPRequest() *io.PipeWriter {
	rd, wr := io.Pipe()

	// TODO: add error checks
	u, _ := url.Parse("http://" + carrier.target + "/txs")
	// u, _ := url.Parse("http://127.0.0.1:8080/txs")

	// Set the content-length to -1 to conform with the chunked encoding
	// requirements: https://golang.org/pkg/net/http/#Request.Write
	// ```
	// If Body is present, Content-Length is <= 0 and TransferEncoding hasn't
	// been set to "identity", Write adds "Transfer-Encoding: chunked" to the
	// header. Body is closed after it is sent.
	// ```
	req := &http.Request{
		Method:           "POST",
		ProtoMajor:       1,
		ProtoMinor:       1,
		URL:              u,
		TransferEncoding: []string{"chunked"},
		Body:             rd,
		ContentLength:    -1,
	}

	// This method blocks until the chunked request finishes, so handle it in
	// a separate routine
	go carrier.outgoingChunkedRequest(req)

	return wr
}

func (carrier *carrier) outgoingChunkedRequest(req *http.Request) {
	// If all goes well, Do() should block
	_, err := carrier.httpClient.Do(req)
	if err != nil {
		glog.Warningf("[%d] Do() chunked request raised error: %s",
			carrier.nr, err)
		req.Body.Close() // This will forbid us from writing anything
	}
}

func (swinger *swinger) run() {
	glog.Infof("[%d] swinger started", swinger.nr)

	// If there's any batch formed, we flush it periodically
	flushTimerChannel := time.Tick(10 * time.Millisecond)

	// We'll keep track of batches using a map.
	// This associates to every client addr (string) a batch (string), where
	// the batch is a concatenation of transaction ids.
	b := make(map[string]string)

	for {
		select {
		case tx := <-swinger.outstanding:
			lbatch := b[tx.Client]
			if lbatch == "" {
				b[tx.Client] = tx.Id
			} else {
				b[tx.Client] = lbatch + "," + tx.Id
			}
		case <-flushTimerChannel:
			for addr, batch := range b {
				req, _ := http.NewRequest("GET", "http://"+addr+":8080/"+batch, nil)
				_, err := swinger.httpClient.Do(req)
				if err != nil {
					glog.Warningf("Do() ACK request raised error: %s", err)
					// We'll retry in the next flush epoch..
				} else {
					// If successful, delete this batch
					delete(b, addr)
				}
			}

		}
	}
}

// A naive serializer
func serializeClientTXRequest(in *client.TXRequest) []byte {
	buf := bytes.NewBufferString(in.Id + "," + in.Client + ",")
	buf.Write(in.Body)
	// We add a terminating \n at the end
	// TODO: make sure this doesn't interfere with HTTP's own chunked encoding
	// mechanism, but it should be fine since chunked transfer encoding relies
	// on \r\n, not just \n
	buf.Write([]byte("\n"))
	out := buf.Bytes()

	return out
}

// A naive deserializer
func deserializeClientTXRequest(in []byte) *client.TXRequest {
	buf := bytes.NewBuffer(in)

	id, err := buf.ReadString(byte(','))
	if err != nil {
		glog.Warningf("Encountered error while deserializing the Id %s", err)
	}
	id = strings.TrimSuffix(id, ",")

	cAddr, err := buf.ReadString(byte(','))
	if err != nil {
		glog.Warningf("Encountered error while deserializing client address %s", err)
	}
	cAddr = strings.TrimSuffix(cAddr, ",")

	body, err := buf.ReadBytes(byte('\n'))
	if err != io.EOF {
		glog.Warningf("Encountered error while deserializing the Body %s", err)
	}

	return &client.TXRequest{
		Id:     id,
		Client: cAddr,
		Body:   body,
	}
}

func ReadJsonConfig(cgfFile string, c *chainrConf) error {
	configData, err := ioutil.ReadFile(cgfFile)
	if err != nil {
		return err
	}

	err = json.Unmarshal(configData, &c)
	if err != nil {
		return err
	}

	// Set the default in case they are missing
	if c.MaxOutstanding <= 0 {
		c.MaxOutstanding = defaultMaxOutstanding
	}
	if c.BodyMaxSizeKB <= 0 {
		c.BodyMaxSizeKB = defaultBodyMaxSizeKB
	}
	if c.RenewalPeriod <= 0 {
		c.RenewalPeriod = defaultRenewalPeriod
	}
	if c.NumCarriers <= 0 {
		c.NumCarriers = defaultNumCarriers
	}

	return nil
}

func newCarrier(nr int, outstandingChanLen int, target string) *carrier {
	return &carrier{
		nr:          nr,
		outstanding: make(chan *client.TXRequest, outstandingChanLen),
		target:      target,
		httpClient: &http.Client{ // HTTP client of the carrier
			Timeout: 0},
	}
}

func newSwinger(id int, outstandingChanLen int) *swinger {
	return &swinger{
		nr:          id,
		outstanding: make(chan *client.TXRequest, outstandingChanLen),
		httpClient: &http.Client{ // HTTP client of the swinger routine
			Timeout: defaultAckTimeoutMs * time.Millisecond},
	}
}

func newNode() *chainrNode {

	var c chainrConf

	flag.StringVar(&c.listenAddr, "listen", defaultListenAddr,
		"`addr:port` of service")
	flag.StringVar(&c.cfgPath, "cfg", "config/chain-node-1.json",
		"`path` to the JSON configuration file")

	flag.Parse()
	err := ReadJsonConfig(c.cfgPath, &c)
	if err != nil {
		glog.Fatalf("Error parsing the JSON config file (%s)", c.cfgPath)
		os.Exit(-1)
	}

	glog.Info("Using configuration parameters:")
	glog.Infof("  -- listening on: %s", c.listenAddr)
	glog.Infof("  -- cfg: %s", c.cfgPath)
	glog.Infof("  -- nextReplicaAddr: %s", c.NextReplicaAddr)
	glog.Infof("  -- MaxOutstanding: %d", c.MaxOutstanding)
	glog.Infof("  -- BodyMaxSizeKB: %d", c.BodyMaxSizeKB)
	glog.Infof("  -- RenewalPeriod: %d", c.RenewalPeriod)
	glog.Infof("  -- NumCarriers: %d", c.NumCarriers)

	node := &chainrNode{
		conf: &c,
		log:  make([]client.TX, 0, logInitialSize),
	}

	if c.NextReplicaAddr != "" {
		node.carriers = make([]carrier, c.NumCarriers)
		for i := 0; i < c.NumCarriers; i++ {
			node.carriers[i] =
				*newCarrier(i, c.MaxOutstanding, c.NextReplicaAddr)
		}
		node.swingers = nil
	} else {
		// For swingers, we'll just use the default values
		// (not critical for performance)
		node.swingers = make([]swinger, defaultNumSwingers)
		for i := 0; i < defaultNumSwingers; i++ {
			node.swingers[i] = *newSwinger(i, defaultMaxOutstanding)
		}
		node.carriers = nil
	}

	return node
}

func (node *chainrNode) run() {
	if node.carriers != nil {
		for i := 0; i < node.conf.NumCarriers; i++ {
			go node.carriers[i].run(
				time.Duration(node.conf.RenewalPeriod),
				node.conf.BodyMaxSizeKB)
		}
	}

	if node.swingers != nil {
		for i := 0; i < defaultNumSwingers; i++ {
			go node.swingers[i].run()
		}
	}

	// Start the HTTP API
	mux := http.NewServeMux()
	mux.Handle("/tx/", node)
	mux.Handle("/txs", node)

	// Ensure graceful tear-down
	graceful.Run(node.conf.listenAddr, tearDownSlack*time.Second, mux)
	glog.Flush()
}

func main() {
	// Create the localNode
	localNode := newNode()

	glog.Info("Starting chain replication system")

	localNode.run()
}
