import requests, threading, sys, time, os, multiprocessing, ctypes, socket
import numpy as np
from multiprocessing import Pool, Manager
import cherrypy

current_milli_time = lambda: int(round(time.time() * 1000))

# How many seconds should the execution last?
EXECUTION_DURATION=20

# How many seconds should we wait for connections initialization
CONNNECTION_SLACK=1

# How many seconds should we wait for outlying executors
EXECUTION_SLACK=6

# Transaction timeout, in seconds
TX_TIMEOUT=5

class ChainrBCFabric(object):
    def __init__(self, ip, port):
        self.service = 'http://' + ip + ":" + str(port) + "/tx/"
        print "Executor initialized -- " + str(os.getpid())

    # Does a transaction
    def tx(self, txId, txPayload):
        url = self.service + str(txId)
        try:
            r = requests.put(url, data=txPayload, timeout=TX_TIMEOUT)
        except requests.exceptions.Timeout, e:
            print "Timeout while executing TX: " + str(e)
        else:
            if (r.status_code != 202):
                print "Error occurred: TX not accepted" + str(r)


class WorkloadResults(object):
    def __init__(self, contactNodeIP="", counter = 0, latencies = []):
        self.opCounter = counter
        self.opLatencies = latencies
        self.runtime = 0
        self.contactIP = contactNodeIP

    def getCounter(self):
        return self.opCounter

    def getLatencies(self):
        return self.opLatencies

    def merge(self, anotherWorkloadRes):
        self.opCounter += anotherWorkloadRes.getCounter()
        self.opLatencies += anotherWorkloadRes.getLatencies()

    def setRuntime(self, time):
        self.runtime = time

    def printStatistics(self):
        if (self.opCounter > 0):
            # avoid division by 0
            throughput = self.opCounter / (float(delta) / 1000)
        else:
            # print 0 if the counter is 0..
            throughput = 0
        print("%10dops\t%10dms\t%8.2f\t%8.2f\t%8.2f\t%8.2f\t%8.2f\t%s" % (
            self.opCounter,
            delta,
            throughput,
            np.mean(self.opLatencies),
            np.std(self.opLatencies),
            np.percentile(self.opLatencies, 95),
            np.percentile(self.opLatencies, 99),
            self.contactIP))

def execute_workload(ip, port, blockSize, startEv, stopEv):
    chainr = ChainrBCFabric(ip, port)
    print "Executor waiting -- " + str(os.getpid())
    startEv.wait()
    print "Executor running -- " + str(os.getpid())
    ## TODO: Perhaps use a random string instead of hardcoded 'x'
    item = "x" * blockSize
    cnt = 0
    lats = []
    while stopEv.is_set() == False:
        startt = current_milli_time()
        txid = str(socket.gethostbyname(socket.gethostname())) + '-' + str(startt) + '-' + str(os.getpid()) + str(cnt)
        # print "tx: " + txid
        chainr.tx(txid, item)
        lats.append(current_milli_time() - startt)
        cnt += 1
    print "Executor finished " + str(cnt) + " -- " + str(os.getpid())
    return WorkloadResults(counter=cnt, latencies=lats)

class ChainrResponses(object):
    @cherrypy.expose
    def index(self):
        return "Hello World!"

if __name__ == '__main__':
    with Manager() as manager:
        startEvent = manager.Event()
        stopEvent = manager.Event()
        executorsCount = int(sys.argv[4])
        print "Using a pool of " + str(executorsCount) + " executors"
        pool = Pool(processes=executorsCount)
        executors = [
            pool.apply_async(
                    execute_workload,
                    [sys.argv[1],               # IP
                    int(sys.argv[2]),           # port
                    int(sys.argv[3]),           # block size
                    startEvent, stopEvent])
            for x in xrange(executorsCount)]
        # Start the web server
        cherrypy.quickstart(ChainrResponses(), '/')
        # Allow executors to establish their connections
        print "Sleeping for " + str(CONNNECTION_SLACK) + " second(s) to establish connections.."
        time.sleep(CONNNECTION_SLACK)
        start_time = current_milli_time()
        startEvent.set()    # start executors
        time.sleep(EXECUTION_DURATION)
        stopEvent.set()     # stop executors
        wkResult = WorkloadResults(contactNodeIP=sys.argv[1])
        delta = current_milli_time() - start_time
        time.sleep(EXECUTION_SLACK)
        for t in executors:
            wr = None
            try:
                wr = t.get(None)
            except:
                print "--Caught it!!"
            if wr is not None:
                print "wr: " + str(wr.getCounter())
                wkResult.merge(wr)
        print " --> total: " + str(wkResult.getCounter())
        wkResult.setRuntime(delta)
        wkResult.printStatistics()

